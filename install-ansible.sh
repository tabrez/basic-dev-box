export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -y
sudo apt-get install -y acl sudo software-properties-common python-is-python3
sudo apt-add-repository -y ppa:ansible/ansible
sudo apt-get install -y ansible
