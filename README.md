# Basic, headless development box

## Introduction

Basic development box built using Vagrant & Ansible with the following features:

+ Ubuntu 21.10, zsh
+ Python 3.10, Poetry
+ NVM, Nodejs 16.14
+ npm global packages like yarn, pnpm, rush, typescript, eslint, prettier, jest, create-react-app fastify-cli

## Boot VM on Windows via WSL2

### (Option 1) Fix ssh file permissions to use ssh client from Windows

[Help](https://github.com/PowerShell/Win32-OpenSSH/wiki/Security-protection-of-various-files-in-Win32-OpenSSH)

### (Option 2 - recommended) Use ssh client from Git for Windows

[Download](https://gitforwindows.org/)

You can select all default settings during installation.

Add the following line in VS Code `settings.json` file to use ssh client from
Git for Windows instead of default openssh client implementation of Windows:

```json
  "remote.SSH.path": "C:\\Program Files\\Git\\usr\\bin\\ssh.exe"
```

### Create ssh key with no passphrase

```sh
# export KEY_NAME={ssh key name}
# curl gen-keys
./gen-keys.sh
```

### ssh config file

Copy the contents to `~\.ssh\config` file

```ssh
Host *
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
    # change `no` to `yes` to ssh using password `vagrant`
    PasswordAuthentication no
Host vagrant
    # use the IP address of the VM, present in `Vagrantfile`
    HostName 192.168.56.11
    User vagrant
    ### comment the following line if you want to use a different private key
    ### fix the path based on where the vagrant project root folder is(relative paths don't work)
    IdentityFile <repo-folder>/.vagrant/machines/default/virtualbox/private_key
    ### uncomment the following line if you want to use your default private key
    # IdentityFile ~/.ssh/id_rsa
    ### uncomment the following line if you want to use key generated by `gen-keys` 
    # IdentityFile ~/.ssh/id_ed25519_devti
```

## Connect VS Code to VM via ssh

```sh
code --folder-uri "vscode-remote://ssh-remote+vagrant/home/vagrant"
```

## Install plugins

```sh
# technically supposed to help with caching apt pacakges during development
vagrant plugin install vagrant-cachier
# this plugin might help if synced folders are not working
vagrant plugin install vagrant-vbguest
# needed to work with vmware provider
vagrant plugin install vagrant-vmware-desktop
```

[Download](https://www.vagrantup.com/vmware/downloads) and install Vagrant
VMWare utility if using VMWare as the provider.

## Troubleshooting

### Recover from errors starting the VM

+ Always start the VM using vagrant commands, not directly from Virtualbox
GUI(many features won't work otherwise, like synced folders)
+ Halt VM before destroying & re-creating the VMs so the virtual machine folders
managed by Virtualbox are properly destroyed

```sh
vagrant halt ; Start-Sleep -s 15 ; vagrant destroy -f ; vagrant up
```

+ Click on "Show" button in Virtualbox GUI for the VM if vagrant gets stuck on
private key auth step; alternatively, try uncommenting `vb.gui = true` line in
`Vagrantfile`
+ Try removing `~\.vagrant.d\insecure_private_key` if getting
SSH errors and restart VM
+ Remove .vagrant.d & .vagrant folders to reset everything

E.g. error: "Guest-specific operations were attempted on a machine that is not ready for guest communication"

```sh
rm -Recurse ~\.vagrant.d
rm -Recurse .\.vagrant
```

### Connecting from WSL

if running `vagrant up` from WSL:

```sh
vagrant plugin install virtualbox_WSL2
# maybe put this in .bashrc/.zshrc
export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"
chmod 600 ~/.ssh/config
chmod 600 <repo folder>/.vagrant/machines/default/virtualbox/private_key
# or if you're using your private key to connect to the VM
chmod 600 ~/.ssh/id_rsa
```

Use `ssh vagrant` instead of `vagrant ssh` to connect to the VM from WSL if
VM was created from Windows/Powershell. Config for `vagrant` name should be in
`~/.ssh/config`

Use the following code for `~/.ssh/config` file

```ssh
Host *
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
    PasswordAuthentication no
Host vagrant
    HostName 192.168.56.11
    User vagrant
    # change based on the location where the repo is cloned
    IdentityFile /mnt/c/Users/<username>/<repo folder>/.vagrant/machines/default/virtualbox/private_key
```

Run the `chmod` command if you get ssh key permission error before `ssh` command:

```sh
chmod 600 <repo folder>/.vagrant/machines/default/virtualbox/private_key
ssh vagrant
```
